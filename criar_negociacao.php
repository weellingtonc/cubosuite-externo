<?php
// Sempre confira a documentação oficial
// Para garantir que não houve alterações nos parâmetros ou endpoints.
// Tudo que esta entre asteriscos (*) precisa ser substituido.

header('Content-Type: application/json');

// Decodifica o JSON recebido na requisição POST
$request = json_decode(file_get_contents('php://input'), true);

// Registra os dados recebidos em um arquivo de log
file_put_contents('criar_negociacao:dados_recebidos.log', print_r($_POST, true));

$parametros = [
    "title" => $_POST['*titulo_da_negociacao*'],
    "pipeId" => 1, // ID do funil
    "peopleName" => $_POST['*nome_pessoa_da_negociacao*'],
    "peoplePhone" => $_POST['*telefone_pessoa_da_negociacao*'],
    //Os campos personalizados são opcionais, caso não queira enviar, basta remover a linha, e caso queira adicionar os ids estão dentro do crm em configurações > campos personalizados
    "customfields" => [
        ["customfieldId" =>  "*id_do_campo_personalizado*", "value" => $_POST['*campo_personalizado_negociacao*'] ? $_POST['*campo_personalizado_negociacao*'] : 'Sem Informação'],
    ]
];

// Inicia a requisição para o Cubo Suite
$url = "https://api.cubosuite.com.br/deals/external";
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parametros));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Para pegar o token da sua conta acesse o crm e vá em meu perfil > api
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer *token*'));
$head = curl_exec($ch);
$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

// Registra a resposta em um arquivo de log
$json = json_decode($head, true);
file_put_contents('criar_negociacao:resultado.log', print_r($json, true));

// Retorna a resposta
$response = json_encode($json);
echo $response;
return $response;

